#include <stdio.h>

#define SWAP(x, y) \
    int tmp = x; \
    x = y; \
    y = tmp;

int main() {
    int a = 3;
    int b = 5;
    printf("a: %d, b: %d\n", a, b);
    SWAP(a, b);
    printf("a: %d, b: %d\n", a, b);
    return 0;
}