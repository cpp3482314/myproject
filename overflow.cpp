#include <iostream>
using namespace std;

int main() {
    int a = 2147483647;
    int b = a + 1;
    cout << b << endl;  // Output: -2147483648

    return 0;
}